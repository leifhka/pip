/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.helene.pip;

import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;

/**
 *
 * @author helene
 */
public class Pip {
    
    public static void main(String[] args) {
        
        ImagePlus imp = IJ.openImage("/home/helene/Desktop/testImage.JPG");
        ImageProcessor ip = imp.getProcessor();
        ImageProcessor newIp = ip.rotateLeft();
        ImagePlus newImage = new ImagePlus("Jul med din glede!", newIp);
        newImage.show();
        System.out.println("Pip in the making");
    }
    
}
